//
//  ViewController.swift
//  SDWebImageDemo
//
//  Created by Edy Cu Tjong on 6/10/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let imageUrl = URL(string: "https://codewithchris.com/img/logo.png")!
        imageView.sd_setImage(with: imageUrl as URL)
    }


}

